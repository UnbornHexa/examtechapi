<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/helper/db.php';

$app = new \Slim\App;

//Carrega Middlewares
require __DIR__ . '/helper/CORS_helper.php';

//Carrega as Rotas
require __DIR__ . '/route/index.php';
require __DIR__ . '/route/movimentacao.php';
require __DIR__ . '/route/usuario.php';
require __DIR__ . '/route/token.php';

$app->run();
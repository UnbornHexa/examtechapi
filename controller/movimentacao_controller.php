<?php
//Dependências
require __DIR__ . '/../model/movimentacao_model.php';
//Registrando alias para a classe para não confundir os métodos
use \MovimentacaoModel as movimentacao_model;

class MovimentacaoController{
	/***********
	*
	*  RECEBE TODAS AS MOVIMENTAÇÕES DO USUARIO
	*
	************/
	public function getAllMovimentacaoDeUsuario($request,$response, $args){
		$model = new movimentacao_model;
		$id_usuario = $request->getAttribute('id_usuario');
		return $model->getAllMovimentacaoDeUsuario($id_usuario);
	}

	/***********
	*
	*  REALIZA TRANSACAO
	*
	************/
	public function realizarTransacao($request,$response, $args){
		$model = new movimentacao_model;
		//Obtendo Parametros da Request
		$vl_movimentacao = $request->getParam('vl_movimentacao');
		$descricao_movimentacao = $request->getParam('descricao_movimentacao');
		$tipo_movimentacao = $request->getParam('tipo_movimentacao');
		$Usuario_id_usuario = $request->getParam('Usuario_id_usuario');
		$email_destinatario = $request->getParam('email_destinatario');
		//Inserindo parametros na variavel data
		$data = [
			'vl_movimentacao'=>$vl_movimentacao,
			'descricao_movimentacao'=>$descricao_movimentacao,
			'tipo_movimentacao'=>$tipo_movimentacao,
			'Usuario_id_usuario'=>$Usuario_id_usuario,
			'email_destinatario'=>$email_destinatario
		];

		//Envia variavel $data ao método e retorna resultado
		return $model->realizarTransacao($data);
	}



}

?>
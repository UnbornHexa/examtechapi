<?php
//Dependencias
require __DIR__ . '/../model/token_model.php';
//Registrando alias para a classe para não confundir os métodos
use \TokenModel as token_model;
use \Firebase\JWT\JWT;


class TokenController{
	/***********
	*
	*  RECEBE UM TOKEN POR ID USUARIO
	*
	************/
	public function getToken($request,$response, $args){
		$model = new token_model;
		$id = $request->getAttribute('id');
		return $model->getToken($id);
	}

	/***********
	*
	*  CRIA TOKEN
	*
	************/
	public function addToken($request,$response, $args){
		$model = new token_model;
		//Obtém parametros
		$login = $request->getParam('login_usuario');
		$senha = $request->getParam('senha_usuario');
		//Coloca parametros no array
		$data = [
			'login_usuario'=>$login,
			'senha_usuario'=>$senha
		];
		//Envia data para o model para obter id do usuario logado
		$id_usuario_logado = $model->autenticaUsuario($data);

		//Testa se existe resultado
		if (!isset($id_usuario_logado)) {
				//Retorna a falta de resultado
		        $dataResultado = ["isError" => true,"mensagem" => 'Usuario não registrado'];
		        return json_encode($dataResultado);
		    } else {
		    		//Testa se existe um token no id deste usuario
		    		$token_atual = $model->encontraToken($id_usuario_logado);
			    		//Testa se o token existente na DB ainda é válido
			    		if($token_atual->expiracao_token < time()){
			    			$model->delToken($id_usuario_logado);
			    			$token_atual = null;
			    		}
			    		$data = [
			    			'login_usuario'=>$login,
			    			'senha_usuario'=>$senha,
			    			'id_usuario_logado'=>$id_usuario_logado,
			    			'token_atual'=>$token_atual
			    		];
			        // Cria um novo token se usuario existir mas não houver um token associado a ele.
		    			return $model->addToken($data);
		    }
	}

	/***********
	*
	*  DELETA TOKEN
	*
	************/
	public function delToken($request,$response, $args){
		$model = new token_model;
		$id = $request->getAttribute('id');
		return $model->delToken($id);
	}
}
?>
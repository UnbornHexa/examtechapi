<?php
//Dependencias
require __DIR__ . '/../model/usuario_model.php';
//Registrando alias para a classe para não confundir os métodos
use \UsuarioModel as usuario_model;
use \MovimentacaoModel as movimentacao_model;
use \TokenModel as token_model;

class UsuarioController{
	/***********
	*
	*  RECEBE TODOS OS USUARIOS
	*
	************/
	public function getAllUsuario($request,$response, $args){
		$model = new usuario_model;
		return $model->getAllUsuario();
	}

	/***********
	*
	*  RECEBE UM USUARIO POR ID
	*
	************/
	public function getUsuario($request,$response, $args){
		$model = new usuario_model;
		$id = $request->getAttribute('id');
		return $model->getUsuario($id);
	}

	/***********
	*
	*  RECEBE UM USUARIO POR EMAIL
	*
	************/
	public function getEmail($request,$response, $args){
		$model = new usuario_model;
		$email = $request->getAttribute('email');
		return $model->getEmail($email);
	}

	/***********
	*
	*  ADICIONA USUARIO
	*
	************/
	public function addUsuario($request,$response, $args){
		$model = new usuario_model;
		//Obtendo Parametros da Request
		$nm_usuario = $request->getParam('nm_usuario');
		$email_usuario = $request->getParam('email_usuario');
		$login_usuario = $request->getParam('login_usuario');
		$senha_usuario = password_hash($request->getParam('senha_usuario'), PASSWORD_DEFAULT);

		//Inserindo parametros na variavel data
		$data = [
			'nm_usuario'=>$nm_usuario,
			'email_usuario'=>$email_usuario,
			'login_usuario'=>$login_usuario,
			'senha_usuario'=>$senha_usuario
		];
		//Envia variavel $data ao método e retorna resultado
		return $model->addUsuario($data);
	}

	/***********
	*
	*  EDITA USUARIO
	*
	************/
	public function editUsuario($request,$response, $args){
		$model = new usuario_model;
		//Obtendo Parametros da Request
		$id = $request->getAttribute('id');
		$nm_usuario = $request->getParam('nm_usuario');
		$email_usuario = $request->getParam('email_usuario');
		$senha_usuario = password_hash($request->getParam('senha_usuario'), PASSWORD_DEFAULT);
		//Inserindo parametros na variavel data
		$data = [
			'nm_usuario'=>$nm_usuario,
			'email_usuario'=>$email_usuario,
			'senha_usuario'=>$senha_usuario
		];
		//Envia variavel $data ao método e retorna resultado
		return $model->editUsuario($data, $id);
	}

	/***********
	*
	*  DELETA USUARIO
	*
	************/
	public function delUsuario($request,$response, $args){
		$model_usuario = new usuario_model;
		$model_token = new token_model;
		$model_movimentacao = new movimentacao_model;
		$id = $request->getAttribute('id');
		$model_token->delToken($id);
		$model_movimentacao->delMovimentacao($id);
		return $model_usuario->delUsuario($id);
	}

	/***********
	*
	*  RECEBE SALDO DO USUARIO
	*
	************/
	public function getSaldo($request,$response,$args){
		$model = new usuario_model;
		//
		$id = $request->getAttribute('id');
		return $model->getSaldo($id);
	}

	/***********
	*
	*  EDITA SALDO DO USUARIO
	*
	************/
	public function editSaldo($request,$response,$args){
		$model = new usuario_model;
		$id = $request->getAttribute('id_usuario');
		$valor = $request->getParam('valor');
		return $model->editSaldo($id,$valor);
	}
}
?>
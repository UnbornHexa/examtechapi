<?php
	class MovimentacaoModel{
		public function getAllMovimentacaoDeUsuario($id_usuario){
				$sql = "SELECT * FROM movimentacao WHERE Usuario_id_usuario = :id_usuario";
				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);
					$stmt->bindParam(':id_usuario',$id_usuario);
					$stmt->execute();
					$movimentacao = $stmt->fetchALL(PDO::FETCH_ASSOC);
					return json_encode($movimentacao);
				}
				catch(PDOException $e){
					$dataResultado = ["isError" => true,"mensagem" => 'Falha em obter Extrato'];
		            return json_encode($dataResultado);
				}
			}

		public function realizarTransacao($data){
			//Definindo sql para a criação de movimentação
			$sql = "INSERT INTO movimentacao (vl_movimentacao,descricao_movimentacao,tipo_movimentacao,Usuario_id_usuario) VALUES(:vl_movimentacao,:descricao_movimentacao,:tipo_movimentacao,:Usuario_id_usuario)";

			//Definindo sql para a retirada de saldo do usuario
			$sql2 = "UPDATE usuario SET saldo_usuario = saldo_usuario - :saldo_usuario WHERE id_usuario = :id_usuario";

			//Definindo sql para o aumento de saldo do destinatário
			$sql3 = "UPDATE usuario SET saldo_usuario = saldo_usuario + :saldo_usuario WHERE email_usuario = :email_destinatario ";

				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();
					//Inicia a transação
					$db->beginTransaction();
					//Prepara o primeiro statement
					$stmt = $db->prepare($sql);
					//Bindando parametros com os valores das variaveis
					$stmt->bindParam(':vl_movimentacao', $data['vl_movimentacao']);
					$stmt->bindParam(':descricao_movimentacao', $data['descricao_movimentacao']);
					$stmt->bindParam(':tipo_movimentacao', $data['tipo_movimentacao']);
					$stmt->bindParam(':Usuario_id_usuario', $data['Usuario_id_usuario']);
					$stmt->execute();
					
					//Prepara a segunda statement
					$stmt2 = $db->prepare($sql2);
					//Bindando parametro com o valor da variavel
					$stmt2->bindParam(':saldo_usuario', $data['vl_movimentacao']);
					$stmt2->bindParam(':id_usuario', $data['Usuario_id_usuario']);
					$stmt2->execute();
					//Prepara a terceira statement
					$stmt3 = $db->prepare($sql3);
					//Bindando parametro com o valor da variavel
					$stmt3->bindParam(':saldo_usuario', $data['vl_movimentacao']);
					$stmt3->bindParam(':email_destinatario',$data['email_destinatario']);
					//Executando os statement
					$teste3 = $stmt3->execute();
					//Finaliza a transação
					$db->commit();
					//Retorna mensagem de sucesso
					$dataResultado = ["isError" => false,"mensagem" => 'Transação Realizada'];
		            return json_encode($dataResultado);
				}
				catch(PDOException $e){
					//Reverte as alterações em caso de erro e retorna mensagem de erro
					$db->rollback();
					$dataResultado = ["isError" => true,"mensagem" => 'Falha na Transação'];
		            return json_encode($dataResultado);
					
				}

			}

			//Método para deletar movimentações quando usuario é excluido
			public function delMovimentacao($id){
				$sql = "DELETE FROM movimentacao WHERE Usuario_id_usuario = :id_usuario";
				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);
					$stmt->bindParam(':id_usuario', $id);
					$stmt->execute();
					$dataResultado = ["isError" => false,"mensagem" =>'Movimentações Apagadas'];
			                return json_encode($dataResultado);
				}
				catch(PDOException $e){
					$dataResultado = ["isError" => true,"mensagem" => 'Falha em apagar Movimentações'];
			        return json_encode($dataResultado);
				}
			}



		}
?>
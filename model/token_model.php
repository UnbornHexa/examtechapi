<?php
	use \Firebase\JWT\JWT;
	class TokenModel{

		/***********
		*
		*  VERIFICA SE USUARIO EXISTE E OBTEM SEU ID
		*
		************/
		public function autenticaUsuario($data){
			//SQL para obter a hasshed password do usuario que está logando
			$sql = "SELECT senha_usuario FROM usuario WHERE login_usuario = :login_usuario";
			$verificado = null;
			try{
				//Get objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();
				//Recebendo o valor hasshed da senha do banco de dados
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':login_usuario', $data['login_usuario']);
				$stmt->execute();
				$resultado = $stmt->fetch();
				//Verifica se a senha enviada é a mesma do banco,
				if($resultado != null){
				$verificado = password_verify($data['senha_usuario'],$resultado->senha_usuario);
				}
				$db = null;
				}
				catch(PDOException $e){
						$dataResultado = ["isError" => true,"mensagem" => 'Usuario ou senha incorretas'];
		                return json_encode($dataResultado);
					}
				//Senha é a mesma do banco de dados
				if($verificado == true){
					//Pegando o id do usuario logado
					$sql2 = "SELECT id_usuario FROM usuario WHERE login_usuario = :login_usuario AND senha_usuario = :senha_usuario";
					try{
						//Get Objeto DB
						$db = new db();
						//Connect
						$db = $db->connect();
						//Executando Statement para obter o id do usuario logado
						$stmt = $db->prepare($sql2);
						$stmt->bindParam(':login_usuario', $data['login_usuario']);
						$stmt->bindParam(':senha_usuario', $resultado->senha_usuario);
						$stmt->execute();
						$id_usuario_logado = $stmt->fetchAll();
						$db = null;
						//retorna id do usuario logado para a variavel no controller
						return $id_usuario_logado[0]->id_usuario;
					}
					catch(PDOException $e){
						$dataResultado = ["isError" => true,"mensagem" => 'Falha em obter id do usuario'];
		                return json_encode($dataResultado);
					}
				}

			}

		/***********
		*
		*  VERIFICA SE EXISTE TOKEN COM ID DE USUARIO LOGADO
		*
		************/

		public function encontraToken($id_usuario_logado){
			// Encontra um token correspondente ao usuario logado.
		        $sql = "SELECT * FROM token
		            WHERE Usuario_id_usuario = :id_usuario_logado AND expiracao_token >" . time();

		        $token_banco = null;
		        try {
		        	$db = new db();
		            $db = $db->connect();
		            $stmt = $db->prepare($sql);
		            $stmt->bindParam(":id_usuario_logado", $id_usuario_logado);
		            $stmt->execute();
		            $token_banco = $stmt->fetchObject();
		            $db = null;
		            //Se Token_banco não estiver vazio
		            if ($token_banco) {
		                return $token_banco;
		            }
		        } catch (PDOException $e) {
		            $dataResultado = ["isError" => true,"mensagem" => 'Falha em consultar token existente'];
		            return json_encode($dataResultado);
		        }
		}

		/***********
		*
		*  ADICIONAR TOKEN
		*
		************/

		public function addToken($data){

			//Se houver dados em Data e Token_atual for nulo
			if (count($data) > 0 && !$data['token_atual']) {

		            $payload = array(
		                "iss"     => "http://examtech.com",
		                "iat"     => time(),
		                "exp"     => time() + (3600 * 24 * 15),
		                "context" => [
		                    "user" => [
		                        "login_usuario" => $data['login_usuario'],
		                        "id_usuario"    => $data['id_usuario_logado']
		                    ]
		                ]
		            );

		            //Transformando Payload e Secret em JWT
		            try {
		            	$db = new db();
		            	$key = $db->secret_key;
		            	$db = null;
		                $jwt = JWT::encode($payload, $key);
		            } catch (Exception $e) {
		                $dataResultado = ["isError" => true,"mensagem" => 'Falha em gerar Token'];
		            return json_encode($dataResultado);
		            }
		            //Dividindo o JWT para salvamento no banco

		            //Inserindo JWT no Banco de Dados
		            $sql = "INSERT INTO token (jwt_token, criacao_token, expiracao_token,Usuario_id_usuario)
		                VALUES (:jwt_token, :criacao_token, :expiracao_token, :Usuario_id_usuario)";
		            try {
		            	$db = new db();
		            	$db = $db->connect();
		                $stmt = $db->prepare($sql);
		                $stmt->bindParam(":jwt_token", $jwt);
		                $stmt->bindParam(":criacao_token", $payload['iat']);
		                $stmt->bindParam(":expiracao_token", $payload['exp']);
		                $stmt->bindParam(":Usuario_id_usuario", $payload['context']['user']['id_usuario']);
		                $stmt->execute();
		                $db = null;
		                //Retorna o token e o login do usuario logado
		                $dataResultado = [
		                    "token"      => $jwt,
		                    "id_usuario_logado" => $payload['context']['user']['id_usuario'],
		                    "login_usuario" => $payload['context']['user']['login_usuario'],
		                    "isError" => false
		                ];
		                return json_encode($dataResultado);
		            } catch (PDOException $e) {
		            	$dataResultado = ["isError" => true,"mensagem" => 'Falha em salvar token na Database'];
		                return json_encode($dataResultado);
		            }
		        }
		        //Se houver um token atual válido
		        else{
		        		$dataResultado = [
		                    "token"      => $data['token_atual']->jwt_token,
		                    "id_usuario_logado" => $data['id_usuario_logado'],
		                    "login_usuario" => $data['login_usuario'],
		                    "isError" => false
		                ];
		                return json_encode($dataResultado);
		        }
		}

		/***********
		*
		*  RECEBE TOKEN POR ID USUARIO
		*
		************/
		public function getToken($id){
				$sql = "SELECT * FROM token WHERE Usuario_id_usuario = :id_usuario";
				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);
					$stmt->bindParam(':id_usuario', $id);
					$stmt->execute();
					$token = $stmt->fetchAll();
					$dataResultado = ["isError" => false,"token" => $token];
		            return json_encode($dataResultado);
				}
				catch(PDOException $e){
					$dataResultado = ["isError" => true,"mensagem" => 'Falha em receber Token do Usuario'];
		            return json_encode($dataResultado);
				}
		}

		/***********
		*
		*  DELETA TOKEN
		*
		************/
		public function delToken($id){
			$sql = "DELETE FROM token WHERE Usuario_id_usuario = :id_usuario";
			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				$stmt->bindParam(':id_usuario', $id);
				$stmt->execute();
				$dataResultado = ["isError" => false,"mensagem" =>'Token Destruido'];
		                return json_encode($dataResultado);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em destruir Token'];
		        return json_encode($dataResultado);
			}
		}

	}
?>
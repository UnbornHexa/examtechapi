<?php

	class UsuarioModel{

		/***********
		*
		*  RECEBE TODOS OS USUARIOS
		*
		************/
		public function getAllUsuario(){
			$sql = "SELECT * FROM usuario";
			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				$stmt->execute();
				$usuarios = $stmt->fetchAll();
				return json_encode($usuarios);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em receber Usuarios'];
		        return json_encode($dataResultado);
			}
		}

		/***********
		*
		*  RECEBE USUARIO POR ID
		*
		************/
		public function getUsuario($id){
				$sql = "SELECT * FROM usuario WHERE id_usuario = :id_usuario";
				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);
					$stmt->bindParam(':id_usuario', $id);
					$stmt->execute();
					$usuario = $stmt->fetchAll();
					return json_encode($usuario[0]);
				}
				catch(PDOException $e){
					$dataResultado = ["isError" => true,"mensagem" => 'Falha em receber usuario'];
		            return json_encode($dataResultado);
				}
		}

		/***********
		*
		*  RECEBE USUARIO POR EMAIL
		*
		************/
		public function getEmail($email){
				$sql = "SELECT * FROM usuario WHERE email_usuario = :email_usuario";
				try{
					//Get Objeto DB
					$db = new db();
					//Connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);
					$stmt->bindParam(':email_usuario', $email);
					$stmt->execute();
					$usuario = $stmt->fetchAll();
					return json_encode($usuario[0]);
				}
				catch(PDOException $e){
					$dataResultado = ["isError" => true,"mensagem" => 'Falha em receber usuario'];
		            return json_encode($dataResultado);
				}
		}

		/***********
		*
		*  ADICIONAR USUARIO
		*
		************/
		public function addUsuario($data){
			$sql = "INSERT INTO usuario (nm_usuario,email_usuario,login_usuario,senha_usuario) VALUES(:nm_usuario,:email_usuario,:login_usuario,:senha_usuario)";

			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				//Binding Parameters para Statement
				$stmt->bindParam(':nm_usuario', $data['nm_usuario']);
				$stmt->bindParam(':email_usuario', $data['email_usuario']);
				$stmt->bindParam(':login_usuario', $data['login_usuario']);
				$stmt->bindParam(':senha_usuario', $data['senha_usuario']);
				
				//Executando Statement
				$stmt->execute();

				$dataResultado = ["isError" => false,"mensagem" => 'Usuario Adicionado'];
		                return json_encode($dataResultado);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em adicionar Usuario'];
		        return json_encode($dataResultado);
			}
		}

		/***********
		*
		*  EDITA USUARIO
		*
		************/
		public function editUsuario($data, $id){
			$sql = "UPDATE usuario SET nm_usuario = :nm_usuario, email_usuario = :email_usuario, senha_usuario = :senha_usuario WHERE id_usuario =".$id;

			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				//Binding Parameters para Statement
				$stmt->bindParam(':nm_usuario', $data['nm_usuario']);
				$stmt->bindParam(':email_usuario', $data['email_usuario']);
				$stmt->bindParam(':senha_usuario', $data['senha_usuario']);
				//Executando Statement
				$stmt->execute();
				$dataResultado = ["isError" => false,"mensagem" => 'Usuario Atualizado'];
		        return json_encode($dataResultado);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em atualizar Usuario'];
		        return json_encode($dataResultado);
			}
		}

		/***********
		*
		*  DELETA USUARIO
		*
		************/
		public function delUsuario($id){
			$sql = "DELETE FROM usuario WHERE id_usuario = :id_usuario";
			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				$stmt->bindParam(':id_usuario', $id);
				$stmt->execute();
				$dataResultado = ["isError" => false,"mensagem" => 'Usuario Deletado'];
		        return json_encode($dataResultado);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em deletar Usuario'];
		        return json_encode($dataResultado);
			}
		}

		/***********
		*
		*  OBTEM SALDO DE USUARIO POR ID
		*
		************/
		public function getSaldo($id){
			$sql = "SELECT saldo_usuario FROM usuario WHERE id_usuario= :id_usuario";
			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				$stmt->bindParam(':id_usuario',$id);
				$stmt->execute();
				$saldo = $stmt->fetchAll();
				return json_encode($saldo[0]->saldo_usuario);
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em obter saldo'];
		        return json_encode($dataResultado);
			}
		}

		/***********
		*
		*  EDITA SALDO DE USUARIO LOGADO
		*
		************/

		public function editSaldo($id,$valor){
			//SQL para modificar o saldo
			$sql = "UPDATE usuario SET saldo_usuario = saldo_usuario + :valor_usuario WHERE id_usuario = :id_usuario";
			//SQL para gerar o depósito
			$sql2 = "INSERT INTO movimentacao (vl_movimentacao,descricao_movimentacao,tipo_movimentacao,Usuario_id_usuario) VALUES(:vl_movimentacao,:descricao_movimentacao,:tipo_movimentacao,:Usuario_id_usuario)";
			try{
				//Get Objeto DB
				$db = new db();
				//Connect
				$db = $db->connect();
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':valor_usuario', $valor);
				$stmt->bindParam(':id_usuario', $id);
				$stmt->execute();

				$descricao_movimentacao = "Deposito em Conta";
				$tipo_movimentacao = 1;
				//Gerando movimentação do Depósito
				$stmt2 = $db->prepare($sql2);
				$stmt2->bindParam(':vl_movimentacao',$valor);
				$stmt2->bindParam(':descricao_movimentacao',$descricao_movimentacao);
				$stmt2->bindParam(':tipo_movimentacao',$tipo_movimentacao);
				$stmt2->bindParam(':Usuario_id_usuario',$id);
				$stmt2->execute();

				$dataResultado = ["isError" => false,"mensagem" => 'Saldo Atualizado'];
		        return json_encode($dataResultado);			
			}
			catch(PDOException $e){
				$dataResultado = ["isError" => true,"mensagem" => 'Falha em editar Saldo'];
		        return json_encode($dataResultado);
			}
		}
	}
?>
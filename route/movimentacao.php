<?php
//Dependências
require ('controller/movimentacao_controller.php');

//Rota para obter o extrato do usuario
$app->get('/api/movimentacao/{id_usuario}', \MovimentacaoController::class . ':getAllMovimentacaoDeUsuario' );

//Rota para realizar transacao
$app->post('/api/movimentacao', \MovimentacaoController::class . ':realizarTransacao' );

?>
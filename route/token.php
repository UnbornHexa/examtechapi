<?php
//Dependências
require ('controller/token_controller.php');

//Rota para ver token
$app->get('/api/token/{id}', \TokenController::class . ':getToken');

//Rota para criar token
$app->post('/api/token', \TokenController::class . ':addToken');

//Rota para destruir token
$app->delete('/api/token/{id}', \TokenController::class . ':delToken');
?>
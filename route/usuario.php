<?php
//Dependencias
require ('controller/usuario_controller.php');

//Rota para obter todos os usuarios
$app->get('/api/usuario', \UsuarioController::class . ':getAllUsuario' );

//Rota para obter um usuario
$app->get('/api/usuario/{id}', \UsuarioController::class . ':getUsuario' );

//Rota para obter usuario por email
$app->get('/api/usuario/email/{email}', \UsuarioController::class . ':getEmail' );

//Rota para adicionar usuario
$app->post('/api/usuario', \UsuarioController::class . ':addUsuario' );

//Rota para atualizar usuario
$app->put('/api/usuario/{id}', \UsuarioController::class . ':editUsuario' );

//Rota para deletar um usuario
$app->delete('/api/usuario/{id}', \UsuarioController::class . ':delUsuario' );

//Rota para obter saldo de um usuario
$app->get('/api/usuario/saldo/{id}', \UsuarioController::class . ':getSaldo' );

//Rota para alterar saldo do usuario logado
$app->put('/api/usuario/saldo/{id_usuario}', \UsuarioController::class . ':editSaldo' );

?>